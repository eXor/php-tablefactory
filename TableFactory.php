<?php

/**
 * 
 * @author Albert Groothedde <albertgroothedde@gmail.com>
 */
class TableFactory {

        private $horizontalHeader = array();
        private $verticalHeader = array();
        private $table = array(array());
        private $hasHorizontalHeader = false;
        private $hasVerticalHeader = false;
        private $columnCount = 0;
        private $rowCount = 0;

        /**
         * 
         * @param array $array Array with values for the horizontal header
         */
        public function horizontalHeader($array) {
                $this->hasHorizontalHeader = true;
                $this->horizontalHeader = $array;
                if (($lenght = max(array_keys($array))) > $this->columnCount) {
                        $this->columnCount = $lenght;
                }
        }

        /**
         * @param array $array Array with values for the vertical header
         */
        public function verticalHeader($array) {
                $this->hasVerticalHeader = true;
                $this->verticalHeader = $array;
                if (($length = max(array_keys($array))) > $this->rowCount) {
                        $this->rowCount = $length;
                }
        }

        /**
         * @param string $value Value you want to add to the horizontal header
         */
        public function appendHorizontalHeader($value) {
                $this->hasHorizontalHeader = true;
                $this->horizontalHeader[] = $value;
                if (($lenght = max(array_keys($this->horizontalHeader))) > $this->columnCount) {
                        $this->columnCount = $lenght;
                }
        }

        /**
         * @param string $value Value you want to add to the vertical header
         */
        public function appendVerticalHeader($value) {
                $this->hasVerticalHeader = true;
                $this->verticalHeader[] = $value;
                if (($lenght = max(array_keys($this->verticalHeader))) > $this->rowCount) {
                        $this->rowCount = $lenght;
                }
        }

        /**
         * @param int $rowId rowId where you want to add the value (horizontal)
         * @param string $value Value to add to the representing row
         */
        public function appendRow($rowId, $value) {
                $this->table[$rowId][] = $value;

                if (($lenght = max(array_keys($this->table[$rowId]))) > $this->columnCount) {
                        $this->columnCount = $lenght;
                }
        }

        /**
         * @param int $columnId columnId where you want to add the value (vertical)
         * @param string $value Value to add to the column
         */
        public function appendColumn($columnId, $value) {
                $this->table[$columnId][] = $value;

                if (($lenght = max(array_keys($this->table[$columnId]))) > $this->rowCount) {
                        $this->columnCount = $lenght;
                }
        }

        /**
         * @param int $rowId rowId where you want to add the values (horizontal)
         * @param array $array Array with values for the representing row
         */
        public function fillRow($rowId, $array) {
                for ($i = 0; $i < count($array); $i++) {
                        $this->table[$rowId][$i] = $array[$i];
                }
                if (($lenght = max(array_keys($this->table[$rowId]))) > $this->columnCount) {
                        $this->columnCount = $lenght;
                }
        }

        /**
         * @param int $columnId columnId where you want to add the value (vertical)
         * @param array $array Array with values for the representing column
         */
        public function fillColumn($columnId, $array) {
                for ($i = 0; $i < count($array); $i++) {
                        $this->table[$i][$columnId] = $array[$i];
                }

                // Track column lenght
                $i = 0;
                for ($i = 0; $i < max(array_keys($array)); $i++) {
                        if (array_key_exists($columnId, $this->table[$i])) {
                                $i++;
                        }
                }

                if (($lenght = $i) > $this->rowCount) {
                        $this->rowCount = $lenght;
                }
        }

        /**
         * @param int $rowId rowId (horizontal)
         * @param array $columnId columnId (vertical)
         * @param string $value Value that you want to add
         */
        public function fillCell($rowId, $columnId, $value) {

                $this->table[$rowId][$columnId] = $value;

                if (($lenght = $rowId) > $this->rowCount) {
                        $this->rowCount = $lenght;
                }

                if (($lenght = $columnId) > $this->columnCount) {
                        $this->columnCount = $lenght;
                }
        }

        public function renderHtml() {
                if ($this->hasHorizontalHeader) {
                        $this->horizontalHeader = $this->fill1dArrayNullWithEmpty($this->horizontalHeader, $this->columnCount);
                        $this->rowCount++;
                }
                if ($this->hasVerticalHeader) {
                        $this->verticalHeader = $this->fill1dArrayNullWithEmpty($this->verticalHeader, $this->rowCount);
                        $this->columnCount++;
                }
                if (!empty($this->table[0])) {
                        $this->table = $this->fill2dArrayNullWithEmpty($this->table);
                }

                $html = '<table>';
                for ($row = 0; $row < $this->rowCount; $row++) {
                        // render horizontal header cells
                        if ($this->hasHorizontalHeader && $row == 0) {

                                $html .= '<tr>';

                                if ($this->hasHorizontalHeader && $this->hasVerticalHeader && $row == 0) {
                                        $html .= $this->generateTh("");
                                }
                                foreach ($this->horizontalHeader as $hh) {
                                        $html .= $this->generateTh($hh);
                                }
                                $html .= '</tr>';
                        }

                        $html .= '<tr>';
                        for ($column = 0; $column < $this->columnCount; $column++) {
                                if (!empty($this->verticalHeader) && $column == 0) {
                                        $html .= $this->generateTh($this->verticalHeader[$row]);
                                }
                                if (!empty($this->table[$row][$column])) {
                                        $html .= $this->generateTd($this->table[$row][$column]);
                                }
                        }
                        $html .= "</tr>\n";
                }
                return $html .= '</table>';
        }

        private function fill1dArrayNullWithEmpty($array1d, $maxLenght) {
                $array = $array1d;
                for ($i = 0; $i < $maxLenght; $i++) {
                        if (!array_key_exists($i, $array)) {
                                $array[$i] = '';
                        }
                }
                return $array;
        }

        private function fill2dArrayNullWithEmpty($array2d) {
                $array = $array2d;

                for ($row = 0; $row <= $this->rowCount; $row++) {
                        for ($column = 0; $column <= $this->columnCount; $column++) {
                                if (!array_key_exists($row, $array)) {
                                        $array[$row][$column] = '';
                                } else {
                                        if (!array_key_exists($column, $array[$row])) {
                                                $array[$row][$column] = '';
                                        }
                                }
                        }
                }
                return $array;
        }

        /**
         * 
         * @param Object $input Can be both an ArrayObject or a plain String
         * @return string td string (e.g.: <td att1="value">value</td>)
         * @throws Exception Variable $array is not an ArrayObject
         */
        private function generateTd($input) {
                if (!is_array($input)) {
                        if (is_string($input))
                                return "<td>$input</td>";
                        xdebug_break();
                        if (is_subclass_of($input, "CWidget"))
                                return "<td>$input</td>";
                        else {
                                throw new Exception('Value $input is neither an ArrayObject nor a String');
                        }
                }
                $td = '<td ';
                foreach ($input as $attribute => $value) {
                        if ($attribute != 'value')
                                $td .= " $attribute=\"$value\"";
                }
//                $td = trim($td);
                $td .= '>';
                if (!is_null($input['value']))
                        $td .= $input['value'];
                $td .= '</td>';

                return $td;
        }

        /**
         * 
         * @param Object $input Can be both an ArrayObject or a plain String
         * @return string td string (e.g.: <td att1="value">value</td>)
         * @throws Exception Variable $array is not an ArrayObject
         */
        private function generateTh($input) {
                if (!is_array($input)) {
                        if (is_string($input))
                                return "<th>$input</th>";
                        else {
                                throw new Exception('Value $input is neither an ArrayObject nor a String');
                        }
                }
                $td = '<th ';
                foreach ($input as $attribute => $value) {
                        if ($attribute != 'value')
                                $td .= " $attribute=\"$value\"";
                }
//                $td = trim($td);
                $td .= '>';
                if (!is_null($input['value']))
                        $td .= $input['value'];
                $td .= '</th>';

                return $td;
        }

}

?>
